import { createApolloServer } from 'meteor/apollo';
import { makeExecutableSchema } from 'graphql-tools';
import ResolutionsSchema from '../../api/resolutions/resolutions.graphql';
import ResolutionResolvers from '../../api/resolutions/resolvers';
import merge from 'lodash/merge';

// hi
// h1

const testSchema = `
    type Query {
        hi: String
        resolutions: [Resolution]
    }
`;

const typeDefs = [
    testSchema,
    ResolutionsSchema
];

const testResolvers = {
    Query: {
        hi() {
            return "Hello Level Up";
         }
    }
};

// Using lodash to merge the two query objects
const resolvers = merge(
    testResolvers,
    ResolutionResolvers
);

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});


createApolloServer({ schema });