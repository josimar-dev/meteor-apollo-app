import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import App from '../../ui/App';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

// Binds our client to GraphQL server
const httpLink = new HttpLink({
    uri: Meteor.absoluteUrl('graphql')
});

// Creates our cache system
const cache = new InMemoryCache();

// Creates our Apollo Client
const client = new ApolloClient({
    link: httpLink,
    cache
});

// Makes GraphQL available in the application
const ApolloApp = () => (
    <ApolloProvider client={client}>
        <App />
    </ApolloProvider>
);

Meteor.startup(() => {
    render(<ApolloApp />, document.getElementById("app"));
})