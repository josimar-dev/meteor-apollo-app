import React from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import ResolutionForm from './ResolutionForm';

const App = ({ loading, resolutions }) => (
    <div>
        <ResolutionForm />
        <ul>
        {!loading ?
            resolutions.map(resolution => (
                <li key={resolution._id}>
                    {resolution.name}
                </li>
            )) : null
        }
        </ul>
    </div>
);

const resolutionsQuery = gql`
    query Resolutions {
        hi
        resolutions {
            _id
            name
        }
    }
`;

export default graphql(resolutionsQuery, {
    props: ({data}) => ({...data})
})(App);